package com.cbg.service;

import com.cbg.pojo.UserBean;
import org.apache.ibatis.session.RowBounds;

import java.util.List;

/**
 * Created by chenboge on 2017/5/12.
 * <p>
 * Email:baigegechen@gmail.com
 * <p>
 * description:
 */
public interface UserService {

    public UserBean getUser(Integer id);

    public int insertUser(UserBean bean);

    public int updateUser(UserBean bean);

    public int deleteUser(Integer id);

    public List<UserBean> queryUsersByPage(String userName, int start, int limit);

}
