package com.cbg.controller;

import com.cbg.pojo.UserBean;
import com.cbg.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Created by chenboge on 2017/5/18.
 * <p>
 * Email:baigegechen@gmail.com
 * <p>
 * description:
 */
@RequestMapping("/user")
@Controller
public class UserController {

    @Autowired
    UserService userService;

    @ResponseBody
    @RequestMapping("/getuser/{id}")
    public UserBean getUser(@PathVariable("id") int id) {
        return userService.getUser(id);
    }

}
