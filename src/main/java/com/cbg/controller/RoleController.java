package com.cbg.controller;

import com.cbg.pojo.RoleBean;
import com.cbg.service.RoleService;
import org.apache.ibatis.session.RowBounds;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

/**
 * Created by chenboge on 2017/5/12.
 * <p>
 * Email:baigegechen@gmail.com
 * <p>
 * description:
 */
@RequestMapping("/role")
@Controller
public class RoleController {

    @Autowired
    RoleService roleService = null;

    
    @ResponseBody
    @RequestMapping("/getRole/{id}")
    public RoleBean getRole(@PathVariable("id") Integer id) {
        long start = System.currentTimeMillis();
        RoleBean bean = roleService.getRole(id);
        long end = System.currentTimeMillis();
        System.out.println(end - start);
        return bean;
    }

    @ResponseBody
    @RequestMapping("/getRoles/{roleName}")
    public List<RoleBean> getRoles(@PathVariable("roleName") String roleName) {
        long start = System.currentTimeMillis();
        List<RoleBean> beans = roleService.queryRolesByPage(null, 0, 5);
        long end = System.currentTimeMillis();
        System.out.println(end - start);
        return beans;
    }

    @ResponseBody
    @RequestMapping("/getRoles/{page}/{pageSize}")
    public List<RoleBean> getlimitRoles(@PathVariable("page") int page, @PathVariable("pageSize") int pageSize) {
        long start = System.currentTimeMillis();
        List<RoleBean> beans = roleService.queryRoleslimit(page, pageSize);
        long end = System.currentTimeMillis();
        System.out.println(end - start);
        return beans;
    }


}
