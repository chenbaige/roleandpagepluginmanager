package com.cbg.dao;

import com.cbg.interceptor.PageParam;
import com.cbg.pojo.RoleBean;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.executor.statement.RoutingStatementHandler;
import org.apache.ibatis.session.RowBounds;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by chenboge on 2017/5/12.
 * <p>
 * Email:baigegechen@gmail.com
 * <p>
 * description:
 */
@Repository
public interface RoleDao {

    public int insertRole(RoleBean bean);

    public int updateRole(RoleBean bean);

    public int deleteRole(Integer id);

    public RoleBean getRole(Integer id);

    public List<RoleBean> queryRolesByPage(String roleName, RowBounds rowBounds);

    public List<RoleBean> queryRoleslimit(@Param("pageParam") PageParam pageParam);
}
